import pygame
from settings import *
import draw
import movement
from maps_base import *



pygame.init()
screen = pygame.display.set_mode(SCREEN_SIZE)
clock = pygame.time.Clock()
draw = draw.Drawing()
player = movement.Player()


while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            exit()
    
    screen.fill(BLACK)
    
    draw.draw_map(screen, current_map)
    player.move(screen)

    pygame.display.flip()
    clock.tick(FPS)