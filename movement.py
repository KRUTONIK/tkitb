import pygame
from settings import *
import draw


def check_pos(x_player_pos, y_player_pos, x_player_pos_old, y_player_pos_old, touchable_blocks):
    for block in touchable_blocks:
        if (x_player_pos in range((block[0] * 32) - 32, block[0] * 32)) and (y_player_pos in range(block[1] * 32, (block[1] * 32) + 32)):
            x_player_pos, y_player_pos = x_player_pos_old, y_player_pos_old



    touchable_blocks = []

    return x_player_pos, y_player_pos, touchable_blocks


class Player():
    def __init__(self):
        self.x, self.y = player_pos_x, player_pos_y
        self.helmet, self.armor = player_helmet, player_armor
        self.map = current_map


    @property
    def pos(self):
        return (self.x, self.y)


    def move(self, screen):
        keys = pygame.key.get_pressed()
        pos_x, pos_y = self.x, self.y

        if keys[pygame.K_w]:
            self.y += -PLAYER_SPEED
        if keys[pygame.K_s]:
            self.y += PLAYER_SPEED
        if keys[pygame.K_a]:
            self.x += -PLAYER_SPEED
        if keys[pygame.K_d]:
            self.x += PLAYER_SPEED

        check_pos(self.x, self.y, pos_x, pos_y, touchable_blocks)

        

        draw.Drawing().draw_player(self.x, self.y, screen, self.helmet, self.armor)
