import pygame
from settings import *
import movement


class Block():
    def __init__(self, screen, x_block_pos, y_block_pos, color):
        pygame.draw.rect(screen, color, pygame.Rect(x_block_pos * 32, y_block_pos * 32, 32, 32), 0)


class Drawing():
    def draw_player(self, x_player_pos, y_player_pos, screen, player_helmet, player_armor):
        pygame.draw.rect(screen, CYAN, pygame.Rect(x_player_pos, y_player_pos, 32, 32), 0)
        
        pygame.draw.rect(screen, player_helmet, pygame.Rect(x_player_pos, y_player_pos, 32, 10), 0)

        pygame.draw.rect(screen, player_armor, pygame.Rect(x_player_pos, y_player_pos + 16, 32, 16), 0)


    def draw_map(self, screen, map):
        world_map = set()

        for y, row in enumerate(map):
            for x, char in enumerate(row):
                if char == 'W':
                    Block(screen, x, y, WHITE)
                    touchable_blocks.append([x, y])

                elif char == 'R':
                    Block(screen, x, y, RED)
                    teleportable_blocks.append([x, y])


