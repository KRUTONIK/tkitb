import pygame
from maps_base import *


SCREEN_SIZE = (1024, 576)
FPS = 60

WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
BLUE = (0, 0, 255)
DARKGRAY = (40, 40, 40)
CYAN = (0, 255, 255)

#equipment
armor1 = RED

helmet1 = GREEN

#player
PLAYER_SPEED = 2
player_pos_x = 48
player_pos_y = 48
player_helmet = helmet1
player_armor = armor1
current_map = map_01

touchable_blocks = []
teleportable_blocks = []
